#!/usr/bin/env python3
"""Make dumps"""
import pynto
import argparse    
import json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    choices = ["expenses", "reports", "settlements", "events", "documents", "all"]  # not events (yet)
    parser.add_argument("what", choices=choices)
    args = parser.parse_args()
    
    dinto = pynto.DintoConnection()
    
    d = {
            "expenses":dinto.expenses,
            "reports":dinto.reports,
            "settlements":dinto.settlements,
            "events":dinto.events,
            "documents":dinto.documents
        }
    todo={}
    if args.what in d:
        todo[args.what]=d[args.what]
    else: 
        todo = d
    for k,v in todo.items():
        with open(k+".json", "w") as f:
            json.dump({k: v()}, f)
