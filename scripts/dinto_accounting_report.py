#!/usr/bin/env python3
"""Exports a given dinto-report to a latex document"""
import pynto
import argparse

parser = argparse.ArgumentParser(description=__doc__)
add = parser.add_argument
add("reportname")
add("--token", default=None, help="Specify apikey on commandline. Optional. Default is to read ~/.dinto_api_key")
add("--token_path", default=None, help="Specify path for apikey. Optional. Default is to read ~/.dinto_api_key")
add("--epilogue", default="", help="File to append as epilogue.")
args = parser.parse_args()


dinto = pynto.DintoConnection(args.token, args.token_path)
epilogue = open(args.epilogue, "r").read() if args.epilogue else ""

report = dinto.get_report(name=args.reportname)

# with open("report_dump.json", "w") as f:
#     json.dump(report, f, indent=2)

template = pynto.templates.latex.get_template("accounting_report.tex")

for u in report["users"]:
    u.update(u["pivot"])
users = [argparse.Namespace(**(pynto.all_currency_to_string(u))) for u in report["users"]]
document = template.render(
    report_label=report["label"],
    preamble="Opdateret, detaljeret udgave kan findes på http://din.to.",
    from_date=report["from_date"],
    to_date=report["to_date"],
    users=sorted(users, key=lambda u: u.name),
    epilogue=epilogue
)

with open("dinto_extract_{}.tex".format(args.reportname), "w") as f:
    f.write(document)
