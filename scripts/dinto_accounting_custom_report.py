#!/usr/bin/env python3
"""Creates a custom report based on transactions, both current and legacy (optional)"""
import pynto
import argparse
from dateutil import parser as dateparser
from datetime import timezone
from collections import defaultdict, deque
import json

class DateRange:
    def __init__(self, name, from_d, to_d):
        self.name = name
        self.from_d = dateparser.parse(from_d).replace(tzinfo=timezone.utc)
        self.to_d = dateparser.parse(to_d).replace(tzinfo=timezone.utc)
        #print("DateRange created with {} to {}".format(self.from_d, self.to_d))
    def __contains__(self, date):
        d = dateparser.parse(date).replace(tzinfo=timezone.utc)
        return (d>=self.from_d) and (d<self.to_d)

class Summarizer:
    def __init__(self, daterange, classes, aliases):
        self.daterange = daterange
        self.name = daterange.name
        self.classes = classes
        self.alias_to_class = aliases
        self.expense_summary = defaultdict(lambda: 0)
        self.classified=0
        self.unclassified=0
        self.withdraws = 0
        self.deposits = 0
        self.fines = 0
        self.fees = 0

    def consume_list(self, items):
        relevant_items = filter(lambda item: item["transaction"]["created_at"] in self.daterange, items)
        for item in relevant_items:
            amount = float(item["amount"])
            ctype = item["context_type"]
            if ctype=="expenses":
                name = item["context"]["name"].strip()
                classif = self.alias_to_class.get(name.lower(), name.lower())
                self.expense_summary[classif] += amount
                if name.lower() in self.alias_to_class:
                    self.classified += amount
                else:
                    self.unclassified += amount
            elif ctype==None:
                ttype = item["type"]
                if ttype=="withdraw":
                    self.withdraws += amount
                elif ttype=="deposit":
                    self.deposits += amount
                elif ttype=="fine":
                    self.fines += amount
                elif ttype=="fee":
                    self.fees += amount

def summarizers_to_table(*summarizers):
    """TODO: this stuff should probably be done in the template"""
    summarizers = sorted(summarizers, key=lambda s: s.name)
    classes_set = set.union(*[set(s.classes) for s in summarizers])
    classes = sorted(classes_set, key=lambda c: max(s.expense_summary.get(c,0) for s in summarizers),
        reverse=True)
    unclassifieds = set.union(*[set(k for k in s.expense_summary if k.lower() not in s.alias_to_class)
        for s in summarizers])
    unclassifieds = sorted(unclassifieds, key=lambda c: max(s.expense_summary.get(c,0) for s in summarizers),
        reverse=True)

    colnames = [s.name for s in summarizers]
    emptyrow=("-----",lambda s: "")
    titlerow=lambda t: (t, lambda s: "")
    rownames=[
        ("Expenses on list", lambda s: s.classified),
        ("Expenses not on list", lambda s: s.unclassified),
        ("Fees in total", lambda s: s.fees),
        ("Fines in total", lambda s: s.fines),
        ("Delta Balance", lambda s: -s.classified-s.unclassified+s.fees+s.fines),
        emptyrow,
        ("Cash deposits", lambda s: s.deposits),
        ("Cash withdrawals", lambda s: s.withdraws),
        ("Delta cash", lambda s: s.deposits-s.withdraws)
    ]

    rownames += [emptyrow,titlerow("Expenses on the list:")]
    for name in classes:
        def f(s, name=name):
            return s.expense_summary.get(name,0)
        rownames += [(name, f)]

    rownames += [emptyrow,titlerow("Expenses not on the list:")]
    for name in unclassifieds:
        def f(s, name=name):
            return s.expense_summary.get(name.lower(),0)
        rownames += [(name, f)]

    rows=[{s.name: pynto.formats.try_convert(rnf(s)) for s in summarizers} for rowname,rnf in rownames]
    for (rowname,rnf), row in zip(rownames,rows):
        row["name"] = rowname.replace("%", "\\%")
    return colnames, rows

def clean_legacy_settlement(settlement):
    """ensure legacy settlement (transaction) has properties like new ones"""
    new_settlement = {
        "amount": settlement["total"],
        "context_type": "expenses" if settlement["type"]=="purchase" else None,
        "transaction":{"created_at":settlement["timestamp"]["$date"]}
    }
    if settlement["type"]!="purchase":
        # {'purchase', 'deposit', 'dinner.guests', 'dinner.chef', 'withdraw', 'transfer', 'fee', 'dinner.eat', 'fine'}
        new_settlement["type"]=settlement["type"]
    else:
        new_settlement["context"]={"name":settlement["where"]["purchase"]["name"]}
    return new_settlement


def get_arguments():
    parser = argparse.ArgumentParser(description=__doc__)
    add = parser.add_argument
    def datetype(arg):
        if len(arg)==4:  # assume year... todo: ensure
            return DateRange(*[arg, "{}-01-01".format(arg), "{}-01-01".format(int(arg)+1)])
        name, dates = arg.split(":", 1)
        return DateRange(*([name]+dates.split(",",1)))
    add("--dateranges", type=datetype, nargs="+", help="Specify dates as eg 2016:2016-01-01,2017-01-01 or just 2016,"
     +"in which case it will autoresolve to previous example")
    add("--legacybak", type=str, default="", help="Malformed JSON file containing old transactions")
    add("--expensealiases", type=str, default="", help="path to json containing aliases for items")
    add("--skipdinto",action="store_true",help="dont connect to din.to (you will then want to use legacy only)")
    args = parser.parse_args()
    return args

def load_settlements(legacybak="",skipdinto=False):
    # todo: filter while loading to avoid loading more than necessary
    if skipdinto:
        dinto_settlements = []
    else:
        dinto_settlements = pynto.DintoConnection().settlements().non_lazy()
    if legacybak:
        old_dinto_settlements = pynto.legacy.Dinto2014Backup(legacybak).settlements()
    else:
        old_dinto_settlements = []
    old_dinto_settlements = list(map(clean_legacy_settlement, old_dinto_settlements))
    return dinto_settlements+old_dinto_settlements

def load_aliases(path):
    with open(path) as f:
        classes = json.load(f)["shoppingitem_aliases"]
    alias2item = {v.lower():k for k,vlist in classes.items() for v in vlist}
    return classes, alias2item

if __name__=="__main__":
    args = get_arguments()
    settlements = load_settlements(legacybak=args.legacybak,skipdinto=args.skipdinto)
    if args.expensealiases:
        classes,alias2item=load_aliases(args.expensealiases)
    else:
        classes,alias2item={},{}
    summarizers = [Summarizer(drange, classes, alias2item) for drange in args.dateranges]
    deque(map(lambda s: s.consume_list(settlements), summarizers), maxlen=0)
    colnames,rows = summarizers_to_table(*summarizers)

    template = pynto.templates.latex.get_template("account_report_custom.tex")
    document = template.render(colnames=colnames, rows=rows)
    with open("dinto_summaries_{}.tex".format("_".join(s.name for s in summarizers)), "w") as f:
        f.write(document)
