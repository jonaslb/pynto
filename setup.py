#!/usr/bin/env python3
from setuptools import setup
from glob import glob
from os.path import join as pjoin
from pathlib import Path

package_files = {
    "pynto": [str(Path(file).relative_to(Path("pynto/"))) for file in
              glob(pjoin("pynto", "templates", "*"))]
}

setup(
    name="pynto",
    version="0.1.2",
    description="Interact with and extract data from din.to",
    packages=["pynto"],
    scripts=glob(pjoin("scripts", "*.py")),
    install_requires=["requests", "jinja2"],
    package_data=package_files
)