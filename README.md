# PYNTO: Dinto Python interface

This tool is an easy to use python interface for dinto (din.to). Nice. 
It also puts the info into latex documents. Even nicer!

## Use
Execute `dinto_accounting_report.py report_name` and watch a latex document being created out of thin air. Magic!

You have to use a token though and best save it under `~/.dinto_api_key` which should contain nothing else. 
You can also supply it as a command line argument though, if you're real tough and don't mind the bash history.

## Installation
Download this repo, `git clone ...` with the address at the top. Then, `./setup.py install` or `./setup.py develop`, depending on how cool you are. 
If you are really uncool and use Windows, you probably use conda for Python. Google `conda source installation`, because I don't want to do it for you.

The project dependencies are `requests` and `jinja2` (they may be installed automatically? I haven't tried *not* having those libs). 
And of course, a sensible Python version (ie. not 2).
