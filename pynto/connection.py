import requests
from requests import get, post, put
from os import path
import sys
import json
from time import sleep
from argparse import Namespace
from collections import deque
from itertools import dropwhile, takewhile
from dateutil import parser as dateparser

_url = lambda url: "http://app.din.to/api/"+url
_urls = Namespace(
    expenses_list=_url("expenses"),
    reports_list=_url("reports"),
    report_get=lambda rep_id: _url("reports/{:d}".format(rep_id)),
    settlements_list=_url("settlements"),
    documents_list=_url("documents"),
    events_list=_url("events"),
)


def _get_token(token_path):
    if token_path is None:
        token_path = path.expanduser(path.join("~", ".dinto_api_key"))
    with open(token_path, "r") as f:
        return f.read().strip()

def daterangepredicate(from_date, to_date):
    f = dateparser.parse(from_date)
    t = dateparser.parse(to_date)
    def inrange(x):
        x = dateparser.parse(x)
        return (x>=f) and (x<t)
    return inrange


class PagedData(list):
    """Class for representing paged data eg. list of reports.
    You should not get by index but iterate to find something"""
    def __init__(self, connection, first_page, base_url=""):
        self.connection = connection
        self.pages = [first_page]
        self.last_page = first_page
        self.full = False
        self.base_url = base_url
        super(PagedData, self).__init__(first_page["data"])

    def __iter__(self):
        i = 0
        while True:
            yield from super(PagedData, self).__getitem__(slice(i, None, None)).__iter__()
            i = len(self)
            next_url = self.last_page["next_page_url"]
            if next_url is None:
                self.full = True
                break
            self.last_page = self.connection._get(self.base_url + next_url).json()
            self.pages += [self.last_page]
            self.extend(self.last_page["data"])

    def non_lazy(self):
        """din.to probably won't like this"""
        if not self.full:
            deque(iter(self), maxlen=0)
        return self

    def __getitem__(self, item):
        # TODO: check if item is in already loaded content
        return super(PagedData, self.non_lazy()).__getitem__(item)

    def take_from_and_while(self, fun):
        """Eg. date filter"""
        return takewhile(fun, dropwhile(lambda x: not fun(x), self))


class DintoConnection:
    def _get(self, *args, **kwargs):
        headers = kwargs.get("headers", {})
        headers.update(self.default_headers)
        kwargs["headers"] = headers
        cookies = kwargs.get("cookies", {})
        cookies.update(self.default_cookies)
        kwargs["cookies"] = cookies
        while True:
            r = requests.get(*args, **kwargs)
            if r.status_code == 200:
                break
            elif r.status_code == 429:
                import warnings
                warnings.warn("Dinto says we are going too fast. Sleeping for 5 seconds.")
                sleep(5)
                continue
            else:
                s = "Got code {}!".format(r.status_code)
                raise requests.HTTPError(s, response=r)
        return r

    def _verify_connection(self, verify_url=_urls.expenses_list):
        """Verify the connection by getting the list of expenses"""
        r = self._get(verify_url)
        if r.status_code == 401:
            s = "Code 401 Unauthorized. Check your api token."
            raise requests.HTTPError(s, response=r)
        elif r.status_code != 200:
            s = "Got code {}!".format(r.status_code)
            raise requests.HTTPError(s, response=r)
        else:
            return True

    def __init__(self, token=None, token_path=None, cookie=None,
                verify=True):
        self.default_headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
        self.default_cookies = {}

        if token is None and cookie is None:
            token = _get_token(token_path)
        elif cookie is not None:
            cookies = cookie.split("; ")
            cookies = dict(pair.strip().split("=",1) for pair in cookies)
            self.default_cookies = cookies
            self.default_headers["X-XSRF-TOKEN"] = cookies["XSRF-TOKEN"]
            #self.default_headers["X-CSRF-TOKEN"] = cookies["CSRF-TOKEN"]
        if token is not None:
            self.default_headers["Authorization"] = "Bearer {}".format(token)
        if verify:
            self._verify_connection()
        # json.dump(self.reports(), open("report_list_dump.json", "w"), indent=2)

    def _paged(self, url):
        # TODO paged data should just take firstpageurl instead of this
        return PagedData(self, self._get(url).json(), url)

    def reports(self):
        return self._paged(_urls.reports_list)

    def settlements(self):
        return self._paged(_urls.settlements_list)

    def expenses(self):
        return self._paged(_urls.expenses_list)

    def documents(self):
        return self._paged(_urls.documents_list)
    
    def events(self):
        return self._paged(_urls.events_list)

    def get_report(self, id=None, name=None):
        """Return a report by name or id. If name is chosen, the first one mathing name in reportname is chosen"""
        if all(arg is None for arg in (name, id)) or all(arg is not None for arg in (name, id)):
            raise TypeError("You must give either name or id but not both.")
        if name is not None:
            reps = self.reports()
            for rep in reps:
                if name in rep["label"]:
                    id = rep["id"]
                    break
            else:
                raise KeyError("Could not find a report with {} in label".format(name))

        return self._get(_urls.report_get(id)).json()
