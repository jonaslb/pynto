"""Contains classes for working with backups of data from old versions of dinto"""
import json

class Dinto2014Backup:
    """Takes a json (that is malformed in that each line is valid json but not
    the entirety).
    Currently only reads transactions json"""
    def __init__(self, transaction_json=None):
        if transaction_json is None:
            raise Exception()
        self.transaction_json_fn = transaction_json
        self.transactions_list = None
        self.expenses_list = None

    def _load(self):
        if self.transactions_list is None:
            with open(self.transaction_json_fn) as f:
                self.transactions_list = list(map(json.loads, f))

    def expenses(self):
        self._load()
        if self.expenses_list is None:
            self.expenses_list = list(filter(lambda t: t["type"]=="purchase", self.transactions_list))
            for e in self.expenses_list:
                e["name"]=e["where"]["purchase"]["name"]
        return self.expenses_list

    def settlements(self):
        self._load()
        return self.transactions_list
