import re
from copy import copy

_d = re.compile(r"-?\d+\.\d+")

def try_convert(v, currency=" kr"):
    if ((type(v) == str) and _d.match(v)) or (type(v) in (float,int)):
        try:
            v = "{:.2f}{}".format(float(v), currency)
        except Exception:
            v = v
    return v


def all_currency_to_string(dictionary, currency=" kr", inplace=False):
    """Takes a dictionary. All strings which are floats are formatted properly"""
    if inplace:
        ret = dictionary
    else:
        ret = copy(dictionary)

    for k, v in ret.items():
        ret[k] = try_convert(v, currency=currency)

    return ret
