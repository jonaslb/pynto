import jinja2
latex = jinja2.Environment(
    loader=jinja2.PackageLoader("pynto", "templates"),
    autoescape=False,
    block_start_string=r"\block{",
    block_end_string="}",
    variable_start_string=r"\var{",
    variable_end_string="}",
    line_statement_prefix="%$",
    line_comment_prefix="%&",
    trim_blocks=True
)